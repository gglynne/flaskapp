from http import HTTPStatus

from flask_testing import TestCase

from app import configure_app
from models import db


class Config:
    SQLALCHEMY_DATABASE_URI = "sqlite://"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    TESTING = True


app = configure_app(Config)


class BaseTest(TestCase):

    def create_app(self):
        return app

    def setUp(self):
        db.create_all()
        self.client = self.app.test_client()

    def tearDown(self):
        db.session.remove()
        db.drop_all()


class ContactTests(BaseTest):

    contact1 = {'emails': 'willy@yahoo.com, willy@google.com',
                'first_name': 'Willy', 'last_name': 'Wonker', }
    contact2 = {'emails': 'willy@yahoo.com',
                'first_name': 'Willy', 'last_name': 'Brandt', }

    def test_add_update_fetch_contact(self):

        response = self.client.get('/contact')
        assert response.status_code == HTTPStatus.OK
        assert response.json == []

        response = self.client.post('/contact/billy', data=self.contact1)
        assert response.status_code == HTTPStatus.CREATED

        response = self.client.get('/contact/willy@google.com')
        assert response.status_code == HTTPStatus.OK

        response = self.client.get('/contact')
        assert response.status_code == HTTPStatus.OK
        assert len(response.json) == 1
        c = response.json[0]
        assert c['first_name'] == self.contact1['first_name']
        assert c['last_name'] == self.contact1['last_name']
        assert c['emails'] == [{'email': 'willy@yahoo.com'},
                               {'email': 'willy@google.com'}]

        response = self.client.put('/contact/billy', data=self.contact2)
        assert response.status_code == HTTPStatus.CREATED
        response = self.client.get('/contact/billy')
        assert response.status_code == HTTPStatus.OK
        assert response.json['emails'] == [{'email': 'willy@yahoo.com'}]

    def test_delete_contact(self):
        response = self.client.get('/contact')
        assert response.status_code == HTTPStatus.OK
        assert response.json == []

        response = self.client.post('/contact/billy', data=self.contact1)
        assert response.status_code == HTTPStatus.CREATED

        response = self.client.delete('/contact/billy')
        assert response.status_code == HTTPStatus.NO_CONTENT
