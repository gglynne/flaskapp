from http import HTTPStatus

from flask_restplus import Resource, abort, fields, reqparse

from api import api
from models import Contact, Email, db

contacts_namespace = api.namespace('/')

ContactParse = reqparse.RequestParser()
for arg in ['emails', 'first_name', 'last_name']:
    ContactParse.add_argument(arg)


email_fields = api.model('Email', {
    'email': fields.String(required=True),
})

contact_fields = api.model('Contact', {
    'username': fields.String(required=True),
    'emails': fields.List(fields.Nested(email_fields)),
    'first_name': fields.String(required=True),
    'last_name': fields.String(required=True),
})


def get_contact(username):
    return Contact.query.filter(Contact.username == username).first()


def abort_if_contact_does_exist(username):
    contact = get_contact(username)
    if contact:
        abort(HTTPStatus.FORBIDDEN,
              message="Contact {} already exists!".format(username))
    return contact


def abort_if_contact_doesnt_exist(username):
    contact = get_contact(username)
    if not contact:
        abort(HTTPStatus.NOT_FOUND.value,
              message="Contact {} doesn't exist".format(username))
    return contact


def abort_if_email_doesnt_exist(email):
    """
    TODO:
    1. Hitting the db twice here, should be able to do it one query...
    2. Multiple contacts with the same email? Needs more work...
    """
    e = Email.query.filter(Email.email == email).first()
    if e:
        contact = Contact.query.filter(Contact.emails.contains(e)).first()
    if (not e) or (not contact):
        abort(HTTPStatus.NOT_FOUND.value,
              message="Contact/Email {} doesn't exist".format(email))
    return contact


@contacts_namespace.route('/contact')
class ContactListView(Resource):
    @contacts_namespace.marshal_with(contact_fields)
    def get(self):
        """ Return a list of all contacts.  """
        contacts = Contact.query.all()
        return contacts, HTTPStatus.OK


@contacts_namespace.route('/contact/<string:username>')
class ContactView(Resource):

    @contacts_namespace.marshal_with(contact_fields)
    def post(self, username):
        """ Save a contact """
        abort_if_contact_does_exist(username)
        data = ContactParse.parse_args()
        data['username'] = username
        contact = Contact(**data)
        db.session.add(contact)
        db.session.commit()
        return contact, HTTPStatus.CREATED

    def delete(self, username):
        """ Deletes a Contact """
        contact = abort_if_contact_doesnt_exist(username)
        for e in contact.emails:
            db.session.delete(e)
        db.session.delete(contact)
        db.session.commit()
        return '', HTTPStatus.NO_CONTENT

    @contacts_namespace.marshal_with(contact_fields)
    def get(self, username):
        """ Returns a contact by username or email  """
        contact = None
        if '@' in username:
            contact = abort_if_email_doesnt_exist(username)
        else:
            contact = abort_if_contact_doesnt_exist(username)
        return contact, HTTPStatus.OK

    @contacts_namespace.marshal_with(contact_fields)
    def put(self, username):
        """ Updates a Contact """
        contact = abort_if_contact_doesnt_exist(username)
        data = ContactParse.parse_args()
        data['username'] = username

        """
        TODO: manage the foreign key relation: there should be a better
        way to do this....
        """
        emails = data.pop('emails')
        emails = [e.strip() for e in emails.split(',')]
        existing = set([e.email for e in contact.emails])
        updated = set(emails)
        to_delete = existing.difference(updated)
        to_add = updated.difference(existing)
        for e in contact.emails:
            if e.email in to_delete:
                db.session.delete(e)
        for email in to_add:
            contact.emails.append(Email(email))

        for k, v in data.items():
            setattr(contact, k, v)
        db.session.commit()
        return contact, HTTPStatus.CREATED.value
