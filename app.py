from datetime import timedelta

from flask import Blueprint, Flask

from api import api
from endpoints import contacts_namespace
from models import db

app = Flask(__name__)


# TODO: move this into a file
class Config:
    SQLALCHEMY_DATABASE_URI = 'sqlite:///datastore.sqlite3'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    CELERY_BROKER_URL = 'redis://localhost:6379/0'
    CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
    CELERYBEAT_SCHEDULE = {
        'run-every-1-minute': {
            'task': 'tasks.create_contacts',
            'schedule': timedelta(seconds=15)
        },
    }


def configure_app(config):
    # config
    app.config.from_object(config)
    # routes
    blueprint = Blueprint('api', __name__, url_prefix='')
    api.init_app(blueprint)
    api.add_namespace(contacts_namespace)
    app.register_blueprint(blueprint)
    # database
    db.init_app(app)
    with app.app_context():
        db.drop_all()
        db.create_all()
    return app


if __name__ == "__main__":
    app = configure_app(Config)
    app.run(debug=True)
