import redis
from celery import Celery
from faker import Faker

from app import Config, configure_app
from models import Contact, db

r = redis.Redis(host='localhost', port=6379, db=0)


def make_celery(app):
    celery = Celery(
        app.import_name,
        backend=app.config['CELERY_RESULT_BACKEND'],
        broker=app.config['CELERY_BROKER_URL']
    )
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)
    celery.Task = ContextTask
    return celery


app = configure_app(Config)
celery = make_celery(app)


contact_prefix = 'fakecontact_'
contact_ex_prefix = 'fakecontact_alive'
expire_after = 15


def create_fake_contact():
    fakedata = Faker()
    emails = ','.join([fakedata.email(), fakedata.email()])
    return Contact(fakedata.user_name(), emails,
                   fakedata.first_name(), fakedata.last_name())


# todo: work out a better way of cascading deletes,
# refactor into delete() method etc...
def delete_expired():
    delete_me = set(r.mget(r.keys(contact_prefix+"*")))
    alive = set(r.mget(r.keys(contact_ex_prefix+"*")))
    deleted = 0
    for username in list(delete_me - alive):
        username = username.decode()
        contact = Contact.query.filter(Contact.username == username).first()
        if contact:
            for email in contact.emails:
                db.session.delete(email)
            db.session.delete(contact)
            deleted += 1
        r.delete(contact_prefix + username)
    db.session.commit()
    return deleted


@celery.task
def create_contacts():
    with app.app_context():
        contact = create_fake_contact()
        db.session.add(contact)
        db.session.commit()

        # remember this contact in redis
        contact_key = contact_prefix + contact.username
        contact_ex_key = contact_ex_prefix + contact.username
        r.set(contact_key, contact.username)
        r.set(contact_ex_key, contact.username, ex=expire_after)

        # delete any expired contacts
        deleted = delete_expired()

        try:
            from sh import notify_send
            notify_send('deleted %s contacts' % deleted)
        except ImportError:
            pass
