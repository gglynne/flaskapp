
.PHONY: rundev celery beat test install freeze


rundev:
	python app.py

celery:
	celery -A tasks.celery worker --concurrency=1

beat:
	celery -A tasks.celery beat -l info

test:
	pytest -s

install:
	pip install -r requirements.txt

freeze:
	pip freeze > requirements.txt
