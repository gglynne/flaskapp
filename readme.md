# setup (e.g. Ubuntu)

```bash
git clone https://bitbucket.org/gglynne/flaskapp.git
cd flaskapp
sudo apt install redis-server
sudo systemctl status redis-server
pip install virtualenvwrapper
mkvirtualenv iqvia
make install
make test

# start celery worker in another terminal
cd flaskapp
workon iqvia && make celery

# start celery beat in another terminal
cd flaskapp
workon iqvia && make beat

# start flask app
make rundev
chromium-browser http://127.0.0.1:5000/

```

# todo

- must be a better way of handling nested models/relationships in sqlalchemy
- celery task needs refactoring after deciding above
- optimize sqlalchemy queries, investigate session.merge vs. session.commit
- move config into files and derive production, dev from a base config
- possibly refactor endpoints into separate files
- database schema needs attention, especially primary keys
- more tests, especially for flaky celery task which is failing after a while (ran out of time) 


# further reading

[Flask SQLAlchemy](https://www.tutorialspoint.com/flask/flask_sqlalchemy.htm)

[REST API Naming Conventions and Best Practices – REST API Tutorial](https://restfulapi.net/resource-naming/)

[Full example Flask-RESTPlus 0.12.1 documentation](https://flask-restplus.readthedocs.io/en/stable/example.html)

[Testing Flask Applications Flask 1.0.2 documentation](http://flask.pocoo.org/docs/1.0/testing/)

[Application Factories Flask 0.12.4 documentation](http://flask.pocoo.org/docs/0.12/patterns/appfactories/#using-applications)






