from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Email(db.Model):
    contact_username = db.Column(
        db.Integer, db.ForeignKey('contact.username'), nullable=False)
    email = db.Column(db.String(100), primary_key=True)


class Contact(db.Model):

    """ Model a Contact with username, email, first name and surname. """

    username = db.Column(db.String(100), primary_key=True)
    emails = db.relationship('Email', backref='contact', lazy=True)
    first_name = db.Column(db.String(100))
    last_name = db.Column(db.String(100))

    def __init__(self, username, emails, first_name, last_name):
        if type(emails) == str:
            emails = [e.strip() for e in emails.split(',')]
            emails = [Email(email=email) for email in emails]
        self.username = username
        self.emails = emails
        self.first_name = first_name
        self.last_name = last_name

    def __repr__(self):
        return '{} ({} {})'.format(
            self.username, self.first_name, self.last_name)
